sonic:
	containerlab deploy --reconfigure --topo sonic.yaml

init-commands:
	docker exec clab-sonic-leaf01 /etc/sonic/run_commands.sh
	docker exec clab-sonic-leaf02 /etc/sonic/run_commands.sh
	docker exec clab-sonic-spine01 /etc/sonic/run_commands.sh
	docker exec clab-sonic-spine02 /etc/sonic/run_commands.sh

init-merge:
	docker exec clab-sonic-leaf01 /etc/sonic/merge.sh
	docker exec clab-sonic-leaf02 /etc/sonic/merge.sh
	docker exec clab-sonic-spine01 /etc/sonic/merge.sh
	docker exec clab-sonic-spine02 /etc/sonic/merge.sh

rvtysh:
	docker exec clab-sonic-leaf01 ln -s /usr/bin/vtysh /usr/bin/rvtysh
	docker exec clab-sonic-leaf02 ln -s /usr/bin/vtysh /usr/bin/rvtysh
	docker exec clab-sonic-spine01 ln -s /usr/bin/vtysh /usr/bin/rvtysh
	docker exec clab-sonic-spine02 ln -s /usr/bin/vtysh /usr/bin/rvtysh

clean:
	containerlab destroy --topo sonic.yaml | true

inspect:
	containerlab inspect --topo sonic.yaml | true
